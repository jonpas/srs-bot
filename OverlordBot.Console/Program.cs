﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Runtime.InteropServices;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RurouniJones.OverlordBot.Core;
using RurouniJones.OverlordBot.Core.Atc;
using RurouniJones.OverlordBot.Core.Awacs;
using RurouniJones.OverlordBot.Core.Monitors;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore;

namespace RurouniJones.OverlordBot.Console
{
    public class Program
    {
        public static void Main(string[] args)
        {
            try
            {
                if (OperatingSystem.IsWindows() && Environment.UserInteractive)
                    ConsoleProperties.DisableQuickEdit();

                Environment.CurrentDirectory = AppDomain.CurrentDomain.BaseDirectory;

                IConfigurationRoot configuration;
                try
                {
                    configuration = new ConfigurationBuilder()
                        .AddYamlFile("configuration.yaml", false, true)
                        .Build();
                }
                catch (Exception ex)
                {
                    if (!Environment.UserInteractive) throw;
                    System.Console.WriteLine("Could not start OverlordBot");
                    System.Console.WriteLine("Error reading \"configuration.yaml\" file");
                    System.Console.WriteLine(ex);
                    throw;
                }
                CreateHostBuilder(args, configuration).Build().Run();
            }
            catch (Exception ex)
            {
                if (!Environment.UserInteractive) throw;
                System.Console.WriteLine("Could not start OverlordBot");
                System.Console.WriteLine(ex);

                if (Environment.UserInteractive)
                {
                    System.Console.ReadKey();
                }
                throw;
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args, IConfigurationRoot configuration)
        {
            var builder = Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<Worker>();
                    services.AddSingleton<Bot>();
                    services.AddSingleton<GameServerFactory>();
                    services.AddTransient<GameServer>();
                    services.AddSingleton<RadioControllerFactory>();
                    services.AddTransient<RadioController>();
                    services.AddTransient<IAirfieldRepository, TacScribeDatabase.AirfieldRepository>();
                    services.AddTransient<IPlayerRepository, TacScribeDatabase.PlayerRepository>();
                    services.AddTransient<IUnitRepository, TacScribeDatabase.UnitRepository>();
                    services.AddTransient<IDataSource, TacScribeDatabase.DataSource>();
                    services.AddTransient<IAirfieldRepository, GrpcDatastore.AirfieldRepository>();
                    services.AddTransient<IPlayerRepository, GrpcDatastore.PlayerRepository>();
                    services.AddTransient<IUnitRepository, GrpcDatastore.UnitRepository>();
                    services.AddTransient<IDataSource, GrpcDatastore.DataSource>();
                    services.AddSingleton<ResponderFactory>();
                    services.AddTransient<IResponder, AwacsResponder>();
                    services.AddTransient<IResponder, AtcResponder>();
                    services.AddTransient<IResponder, MuteResponder>();
                    services.AddSingleton<MonitorFactory>();
                    services.AddTransient<IMonitor, AwacsMonitor>();
                    services.AddTransient<IMonitor, AtcMonitor>();
                    services.Configure<HostOptions>(
                        opts => opts.ShutdownTimeout = TimeSpan.FromSeconds(60));
                    services.Configure<Configuration>(configuration);
                    services.AddOptions();

                });
            if (OperatingSystem.IsWindows())
                builder.UseWindowsService();

            return builder;
        }

        // https://stackoverflow.com/questions/13656846/how-to-programmatic-disable-c-sharp-console-applications-quick-edit-mode
        internal static class ConsoleProperties {

            // STD_INPUT_HANDLE (DWORD): -10 is the standard input device.
            private const int StdInputHandle = -10;

            private const uint QuickEdit = 0x0040;

            [DllImport("kernel32.dll", SetLastError = true)]
            private static extern IntPtr GetStdHandle(int nStdHandle);

            [DllImport("kernel32.dll")]
            private static extern bool GetConsoleMode(IntPtr hConsoleHandle, out uint lpMode);

            [DllImport("kernel32.dll")]
            private static extern bool SetConsoleMode(IntPtr hConsoleHandle, uint dwMode);

            internal static bool DisableQuickEdit() {

                var consoleHandle = GetStdHandle(StdInputHandle);

                GetConsoleMode(consoleHandle, out var consoleMode);

                consoleMode &= ~QuickEdit;

                return SetConsoleMode(consoleHandle, consoleMode);
            }
        }
    }

}
