﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Grpc.Core;
using Grpc.Net.Client;
using NLog;
using RurouniJones.Dcs.Grpc.V0.Trigger;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Encyclopedia;

[assembly: InternalsVisibleTo("OverlordBot.Core.Tests")]

namespace RurouniJones.OverlordBot.Core
{
    public class GameServer
    {
        private readonly HashSet<Task> _gameServerTasks = new();
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        public readonly HashSet<AirfieldStatus> AirfieldStatuses = new();

        public readonly string Name;

        public readonly HashSet<RadioController> RadioControllers = new();
        public readonly string ShortName;

        private readonly Configuration.GameServerConfiguration GameServerConfig;

        private readonly IDataSource DataSource;
        private readonly IAirfieldRepository AirfieldRepository;
        private readonly CancellationToken StoppingToken;

        public GameServer(Configuration.GameServerConfiguration gameServerConfig,
            IDataSource dataSource,
            IAirfieldRepository airfieldRepository,
            IPlayerRepository playerRepository,
            IUnitRepository unitRepository,
            RadioControllerFactory radioControllerFactory,
            ulong discordLogServerId,
            CancellationToken stoppingToken)
        {
            GameServerConfig = gameServerConfig;
            DataSource = dataSource;
            AirfieldRepository = airfieldRepository;
            StoppingToken = stoppingToken;
            Name = gameServerConfig.Name;
            ShortName = gameServerConfig.ShortName;

            foreach (var airfield in Airfield.Entries) AirfieldStatuses.Add(new AirfieldStatus(airfield, ShortName));

            foreach (var controllerConfig in gameServerConfig.Controllers)
                RadioControllers.Add(radioControllerFactory.CreateRadioController(ShortName, airfieldRepository, playerRepository,
                    unitRepository, controllerConfig, gameServerConfig.SimpleRadio, discordLogServerId, AirfieldStatuses));
        }

        public async Task RunAsync()
        {
            using (MappedDiagnosticsLogicalContext.SetScoped("GameServer", $"{ShortName}"))
            {
                var host = GameServerConfig.Host;
                var port = GameServerConfig.Rpc.Port;

                var token = CancellationTokenSource.CreateLinkedTokenSource(StoppingToken).Token;

                _gameServerTasks.Add(RunAirfieldStatusUpdateLoopAsync(token));

                if (DataSource is GrpcDatastore.DataSource grpcDataSource) { 
                    _gameServerTasks.Add(grpcDataSource.StreamUnitUpdatesAsync(token));
                }

                _logger.Info("Starting Controllers");
                foreach (var controller in RadioControllers)
                {
                    _gameServerTasks.Add(controller.RunAsync(token));
                }

                using var channel = GrpcChannel.ForAddress($"http://{host}:{port}");
                var client = new TriggerService.TriggerServiceClient(channel);
                try
                {
                    _logger.Info("Sending startup message via DCS-gRPC");
                                        await client.OutTextAsync(new OutTextRequest
                    {
                        Text = $"OverlordBot {ShortName} starting up",
                        ClearView = false,
                        DisplayTime = 10
                    });
                }
                catch(RpcException ex)
                {
                    _logger.Warn($"Unable to send startup message via DCS-gRPC. #{ex.Message}");
                }
                _logger.Info("Started Gameserver");
                await Task.WhenAll(_gameServerTasks);
                try
                {
                    _logger.Info("Sending shutdown message via DCS-gRPC");
                    await client.OutTextAsync(new OutTextRequest
                    {
                        Text = $"OverlordBot {ShortName} shutting down",
                        ClearView = false,
                        DisplayTime = 10
                    });
                }
                catch (RpcException ex)
                {
                    _logger.Warn($"Unable to send shutdown message via DCS-gRPC. #{ex.Message}");
                }
                _logger.Info("Stopped Gameserver");
            }
        }

        public async Task RunAirfieldStatusUpdateLoopAsync(CancellationToken cancellationToken)
        {
            await Task.Run(async () =>
            {
                _logger.Info("Starting Airfield Update Loop");
                while (cancellationToken.IsCancellationRequested == false)
                    try
                    {
                        foreach (var airfieldUpdate in await AirfieldRepository.GetAllLandAirfields(ShortName))
                        {
                            var airfieldStatus = AirfieldStatuses.FirstOrDefault(status =>
                                status.Name.Equals(airfieldUpdate.Name));
                            if (airfieldStatus == null) continue;
                            airfieldStatus.Coalition = airfieldUpdate.Coalition;
                            if (airfieldUpdate.Heading < 0) continue;
                            airfieldStatus.WindHeading = airfieldUpdate.Heading;
                            airfieldStatus.WindSpeed = airfieldUpdate.Speed;
                        }
                    } catch (RpcException ex)
                    {
                        _logger.Info($"Failed getting airfield status from DCS-gRPC. {ex.Message}");
                    }
                    finally
                    {
                        Thread.Sleep(TimeSpan.FromMinutes(1));
                    }
            }, CancellationToken.None);
            _logger.Info("Stopped Airfield Update Loop");
        }
    }
}
