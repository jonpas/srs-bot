﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Geo.Measure;
using NLog;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Atc.Extensions;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore.Models;
using RurouniJones.OverlordBot.Encyclopedia;
using Stateless;

[assembly: InternalsVisibleTo("OverlordBot.Core.Tests")]

namespace RurouniJones.OverlordBot.Core.Atc
{
    public record ControlledAircraft
    {
        public enum State
        {
            Parked, // Player is currently parked. This is the initial state for someone calling in that they are ready to taxi
            TaxiToRunway, // Player is currently taxiing
            HoldingShort, // Player is holding short. Currently we only do this for runways, not junctions. Player may be waiting to take off or cross
            LinedUpAndWaiting, // Lined up and waiting for take-off clearance
            ClearedForTakeoff, // Have been granted permission to take off
            Rolling, // started their take-off roll
            Outbound, // They are airborne from the runway and outbound
            Flying, // They are flying and not under ATC control. This is also the initial state for players calling into approach for landing
            Inbound, // They are now inbound and under ATC control
            Base, // Started their base leg to the "Initial"
            Final, // Started their final leg
            ShortFinal, // Entered Short final
            Landed, // Have landed on the runway
            TaxiToRamp, // Have left the runway and are taxiing to the ramp
            Aborted // Aborted their approach
        }

        public enum Trigger
        {
            StartTaxi,
            HoldShort,
            LineUpAndWait,
            ClearTakeoff,
            StartTakeoffRoll,
            Takeoff,
            LeaveAtcControl,
            StartInbound,
            TurnBase,
            TurnFinal,
            EnterShortFinal,
            Touchdown,
            LeaveRunway,
            Abort
        }

        private static readonly List<State> StatesRequiringHoldShort = new() {State.Base, State.Final, State.ShortFinal, State.LinedUpAndWaiting, State.ClearedForTakeoff};

        private static readonly List<State> StatesPreventingRunwayCrossing = new()
            {State.Base, State.Final, State.ShortFinal, State.Landed, State.LinedUpAndWaiting, State.ClearedForTakeoff, State.Rolling};

        private static readonly List<State>
            StatesRequiringLineUpAndWait = new() {State.Landed, State.Rolling};

        private readonly Logger _logger = LogManager.GetCurrentClassLogger();

        private readonly RadioController.QueueTransmissionDelegate _queueTransmission;
        private readonly RadioController.LogTransmissionDelegate _logTransmission;


        public readonly AirfieldStatus Airfield;

        public readonly ITransmission.Player PlayerCallsign;
        private CurrentProgressMethod _currentProgressMethod;

        private int _updateMessageSent;

        internal StateMachine<State, Trigger> AtcState;
        public State CurrentState => AtcState.State;

        public readonly List<Airfield.NavigationPoint> NavigationPoints;
        public Airfield.NavigationPoint CurrentNavigationPoint;
        public Airfield.NavigationPoint NextNavigationPoint;
        public Airfield.Runway Destination => (Airfield.Runway) NavigationPoints.Last();

        public List<Airfield.NavigationPoint> NextNavigationPointLine = new();
        
        private const double MetersInMile = 1852.001;
        private const double DistanceBetweenNavigationLinePoints = 0.2 * MetersInMile;

        public Unit PlayerUnit;

        public ControlledAircraft(ITransmission.Player playerCallsign, Unit playerUnit, AirfieldStatus airfield,
            List<Airfield.NavigationPoint> navigationPoints, RadioController.QueueTransmissionDelegate queueTransmission, RadioController.LogTransmissionDelegate logTransmission)
        {
            PlayerCallsign = playerCallsign;
            PlayerUnit = playerUnit;
            Airfield = airfield;
            NavigationPoints = navigationPoints;
            CurrentNavigationPoint = NavigationPoints.First();
            _queueTransmission = queueTransmission;
            _logTransmission = logTransmission;
        }

        private void ConfigureStateMachine()
        {
            AtcState.Configure(State.Parked)
                .Permit(Trigger.StartTaxi, State.TaxiToRunway);

            AtcState.Configure(State.TaxiToRunway)
                .OnEntry(OnEntryStartTaxi)
                .Permit(Trigger.HoldShort, State.HoldingShort)
                .Permit(Trigger.LineUpAndWait, State.LinedUpAndWaiting)
                .Permit(Trigger.ClearTakeoff, State.ClearedForTakeoff);

            AtcState.Configure(State.HoldingShort)
                .OnEntryAsync(OnEntryHoldingShort)
                .Permit(Trigger.LineUpAndWait, State.LinedUpAndWaiting)
                .Permit(Trigger.StartTaxi, State.TaxiToRunway)
                .Permit(Trigger.ClearTakeoff, State.ClearedForTakeoff);

            AtcState.Configure(State.LinedUpAndWaiting)
                .OnEntryAsync(OnEntryLineUpAndWait)
                .Permit(Trigger.ClearTakeoff, State.ClearedForTakeoff);

            AtcState.Configure(State.ClearedForTakeoff)
                .OnEntryAsync(OnEntryClearedTakeoff)
                .Permit(Trigger.StartTakeoffRoll, State.Rolling);

            AtcState.Configure(State.Rolling)
                .OnEntry(OnEntryStartRolling)
                .Permit(Trigger.Takeoff, State.Outbound);

            AtcState.Configure(State.Outbound)
                .OnEntry(OnEntryTakeoff)
                .Permit(Trigger.LeaveAtcControl, State.Flying);

            AtcState.Configure(State.Flying)
                .Permit(Trigger.StartInbound, State.Inbound);

            AtcState.Configure(State.Inbound)
                .OnEntry(OnEntryStartInbound)
                .Permit(Trigger.TurnBase, State.Base)
                .Permit(Trigger.Abort, State.Aborted)
                .PermitReentry(Trigger.StartInbound);

            AtcState.Configure(State.Base)
                .OnEntryAsync(OnEntryTurnBase)
                .Permit(Trigger.Abort, State.Aborted)
                .Permit(Trigger.TurnFinal, State.Final);

            AtcState.Configure(State.Final)
                .OnEntryAsync(OnEntryTurnFinal)
                .Permit(Trigger.Abort, State.Aborted)
                .Permit(Trigger.EnterShortFinal, State.ShortFinal);

            AtcState.Configure(State.ShortFinal)
                .OnEntryAsync(OnEntryEnterShortFinal)
                .Permit(Trigger.Abort, State.Aborted)
                .Permit(Trigger.Touchdown, State.Landed);

            AtcState.Configure(State.Landed)
                .OnEntryAsync(OnEntryTouchdown)
                .Permit(Trigger.Abort, State.Aborted)
                .Permit(Trigger.LeaveRunway, State.TaxiToRamp);

            AtcState.Configure(State.TaxiToRamp)
                .OnEntryAsync(OnEntryLeftRunway);

            AtcState.Configure(State.Aborted)
                .OnEntry(OnEntryAborted)
                .Permit(Trigger.StartInbound, State.Inbound);
            
            AtcState.OnUnhandledTrigger(BadTransition);
        }

        private void BadTransition(State state, Trigger trigger, ICollection<string> u)
        {
            string strings = null;
            if (u != null)
                strings = string.Join(", ", u.ToList());
            LogWarn($"Bad Transition: {state}, {trigger}, {strings}");
        }

        private void LogDebug(string message)
        {
            _logger.Debug($"{PlayerUnit.Id} - {PlayerCallsign}: {message}");
        }

        private void LogWarn(string message)
        {
            _logger.Warn($"{PlayerUnit.Id} - {PlayerCallsign}: {message}");
        }

        public void CalledTaxi()
        {
            AtcState = new StateMachine<State, Trigger>(State.Parked);
            ConfigureStateMachine();

            AtcState.FireAsync(Trigger.StartTaxi);
        }

        public void CalledInbound()
        {
            AtcState = new StateMachine<State, Trigger>(State.Flying);
            ConfigureStateMachine();
            _updateMessageSent = 0;

            AtcState.FireAsync(Trigger.StartInbound);
        }

        public void AbortInbound()
        {
            AtcState.FireAsync(Trigger.Abort);
        }

        /// <summary>
        ///     Only used in testing to set the plane to the desired state.
        /// </summary>
        /// <param name="state"></param>
        internal void SetInitialState(State state)
        {
            AtcState = new StateMachine<State, Trigger>(state);
            ConfigureStateMachine();
        }

        public async Task CheckProgress(Unit playerUnit)
        {
            if (playerUnit == null || playerUnit.Id != PlayerUnit.Id ||
                AtcState.IsInState(State.TaxiToRunway) && Airfield.TaxiPoints.First().DistanceTo(playerUnit, DistanceUnit.Nm) > 5)
            {
                LogDebug("Removing from ATC monitoring");
                Airfield.ControlledPlayers.Remove(PlayerUnit.Id, out _);
                return;
            }

            PlayerUnit = playerUnit;

            await _currentProgressMethod();
        }

        private async Task SendTransmission(string transmission)
        {
            var text = $"{PlayerCallsign.SpokenWithAmbiguity()}, {transmission}";
            _queueTransmission(new Reply{Message = text, ContinueProcessing = true});
            await _logTransmission(text);
        }

        private async Task TransmitHeadingToNextWaypoint(string comment = null)
        {
            var magneticHeading = Regex.Replace(
                Geospatial.TrueToMagnetic(PlayerUnit.Location, PlayerUnit.BearingTo(NextNavigationPoint)).ToString("000"),
                "\\d{1}", " $0");

            await SendTransmission($"fly heading {magneticHeading} for {NextNavigationPoint.Name} {comment}");
        }

        /// <summary>
        /// Updates the current and next navigation points and creates a line of sub-navigation points that allow the pilot to be
        /// off-course more than a single point would allow.
        /// </summary>
        private void UpdateNavigationPoints()
        {
            CurrentNavigationPoint = NavigationPoints.First();
            NextNavigationPoint = NavigationPoints[1];
            NavigationPoints.RemoveAt(0);
            NextNavigationPointLine = new List<Airfield.NavigationPoint> { NextNavigationPoint };
            if (NextNavigationPoint.Name.Contains("Initial"))
            {
                // Create a line of nodes that follow the runway course.

                var bearingToRunway = NextNavigationPoint.BearingTo(Destination);

                // Add the navigation points to the left
                var left = bearingToRunway;
                _logger.Debug($"BearingToRunway: {left}");

                for (var i = 1; i <= 15; i++)
                {
                    var point = Geospatial.CalculatePointFromSource(NextNavigationPoint.Location(), DistanceBetweenNavigationLinePoints * i, left);
                    NextNavigationPointLine.Add(new Airfield.NavigationPoint
                    {
                        Name = $"Left {i}",
                        Latitude = point.Coordinate.Latitude,
                        Longitude = point.Coordinate.Longitude
                    });
                }

                var right = bearingToRunway + 180;
                if (right > 360) right -= 360;

                for (var i = 1; i <= 15; i++)
                {
                    var point = Geospatial.CalculatePointFromSource(NextNavigationPoint.Location(), DistanceBetweenNavigationLinePoints * i, right);
                    NextNavigationPointLine.Add(new Airfield.NavigationPoint
                    {
                        Name = $"Right {i}",
                        Latitude = point.Coordinate.Latitude,
                        Longitude = point.Coordinate.Longitude
                    });
                }
            }
            else
            {
                // Crate a line of nodes perpendicular to the current direction of travel.
                var directionOfTravel = CurrentNavigationPoint.BearingTo(NextNavigationPoint);
                _logger.Debug($"DirectionOfTravel: {directionOfTravel}");


                // Add the navigation points to the left
                var left = directionOfTravel - 90;
                if (left < 0) left += 360;

                for (var i = 1; i <= 15; i++)
                {
                    var point = Geospatial.CalculatePointFromSource(NextNavigationPoint.Location(), DistanceBetweenNavigationLinePoints * i, left);
                    NextNavigationPointLine.Add(new Airfield.NavigationPoint
                    {
                        Name = $"Left {i}",
                        Latitude = point.Coordinate.Latitude,
                        Longitude = point.Coordinate.Longitude
                    });
                }

                var right = directionOfTravel + 90;
                if (right > 360) right -= 360;

                for (var i = 1; i <= 15; i++)
                {
                    var point = Geospatial.CalculatePointFromSource(NextNavigationPoint.Location(), DistanceBetweenNavigationLinePoints * i, right);
                    NextNavigationPointLine.Add(new Airfield.NavigationPoint
                    {
                        Name = $"Right {i}",
                        Latitude = point.Coordinate.Latitude,
                        Longitude = point.Coordinate.Longitude
                    });
                }
            }

            foreach (var nav in NextNavigationPointLine)
            {
                _logger.Debug($"{nav.Latitude},{nav.Longitude}, {nav.Name}, red");
            }
        }

        private delegate Task CurrentProgressMethod();

        #region Trigger Callbacks

        private void OnEntryStartTaxi()
        {
            LogDebug("started Taxi");
            CurrentNavigationPoint = NavigationPoints.First();
            _currentProgressMethod = CheckTaxiProgress;
        }

        private async Task OnEntryHoldingShort()
        {
            await SendTransmission("hold short of runway");
            LogDebug("holding Short");
            _currentProgressMethod = CheckHoldingShort;
        }

        private async Task OnEntryLineUpAndWait()
        {
            await SendTransmission("Line up and wait");
            LogDebug("lined up and waiting");
            _currentProgressMethod = CheckLinedUpAndWaiting;
        }

        private async Task OnEntryClearedTakeoff()
        {
            await SendTransmission($"Take-off {CurrentNavigationPoint.Name} at your discretion");
            LogDebug("cleared for takeoff");
            _currentProgressMethod = CheckClearedTakeoff;
        }

        private void OnEntryStartRolling()
        {
            LogDebug("started takeoff roll");
            _currentProgressMethod = CheckTakeoffRoll;
        }

        private void OnEntryTakeoff()
        {
            LogDebug("airborne");
            Airfield.ControlledPlayers.Remove(PlayerUnit.Id, out _);
        }

        private void OnEntryStartInbound()
        {
            UpdateNavigationPoints();
            LogDebug($"started inbound, current waypoint {CurrentNavigationPoint.Name}, Next waypoint {NextNavigationPoint.Name} bearing {CurrentNavigationPoint.BearingTo(NextNavigationPoint)}");
            _currentProgressMethod = CheckInbound;
        }

        private async Task OnEntryTurnBase()
        {
            UpdateNavigationPoints();
            var altitudeNextWaypoint = NextNavigationPoint.DistanceTo(Destination, DistanceUnit.Nm) * 300;
            await TransmitHeadingToNextWaypoint($", descend {Math.Round(altitudeNextWaypoint / 100, 0, MidpointRounding.AwayFromZero) * 100}, reduce speed your discretion");
            _updateMessageSent = 0;
            LogDebug("turning base");
            _currentProgressMethod = CheckApproachingTurnFinal;
        }

        private async Task OnEntryTurnFinal()
        {
            UpdateNavigationPoints();
            LogDebug("turning final");
            await SendTransmission($"Turn final {Destination.Name}, approach speed and altitude your discretion");
            _currentProgressMethod = CheckApproachingShortFinal;
        }

        private async Task OnEntryEnterShortFinal()
        {
            LogDebug("entering short final");
            await SendTransmission($"Check gear, land {Destination.Name} at your discretion");
            _currentProgressMethod = CheckTouchdown;
        }

        private async Task OnEntryTouchdown()
        {
            LogDebug("touched down");
            await SendTransmission("Exit runway when able");
            _currentProgressMethod = CheckLeftRunway;
        }

        private async Task OnEntryLeftRunway()
        {
            await SendTransmission("Taxi to ramp");
            LogDebug("left runway");
            LogDebug("Removing from ATC monitoring");
            Airfield.ControlledPlayers.Remove(PlayerUnit.Id, out _);
        }

        private void OnEntryAborted()
        {
            LogDebug("Aborted");
            LogDebug("Removing from ATC monitoring");
            Airfield.ControlledPlayers.Remove(PlayerUnit.Id, out _);
        }

        #endregion

        #region Periodic Checks

        private async Task CheckTaxiProgress()
        {
            var closestPoint = NavigationPoints
                .OrderBy(taxiPoint => taxiPoint.DistanceTo(PlayerUnit, DistanceUnit.M))
                .First();

            // If this is true then we don't need to say the same commands again (e.g. permission to cross a runway)
            if (closestPoint == CurrentNavigationPoint)
                return;

            // We want to get rid of all the previous TaxiPoints in the list. We do this instead of just getting rid of the first in case
            // somehow the pilot manage to skip a TaxiPoint by going fast enough that they passed it before the check.
            var index = NavigationPoints.IndexOf(CurrentNavigationPoint);

            if (index > 1)
                LogDebug("skipped at least one taxi point");

            for (var i = NavigationPoints.Count - 1; i >= 0; i--)
            {
                if (i > index) continue;
                LogDebug($"Removing {NavigationPoints[i].Name} from route");
                NavigationPoints.RemoveAt(i);
            }

            CurrentNavigationPoint = closestPoint;
            LogDebug($"New closest TaxiPoint is {CurrentNavigationPoint.Name}");

            if (CurrentNavigationPoint is Airfield.Runway)
            {
                // This is the runway we want to take off from
                if (CurrentNavigationPoint == Destination)
                {
                    if (Airfield.ControlledPlayers.Values.Any(x =>
                        StatesRequiringHoldShort.Contains(x.CurrentState) &&
                        x.Destination == CurrentNavigationPoint))
                    {
                        await AtcState.FireAsync(Trigger.HoldShort);
                    }
                    else if (Airfield.ControlledPlayers.Values.Any(x =>
                        StatesRequiringLineUpAndWait.Contains(x.CurrentState) &&
                        x.Destination == CurrentNavigationPoint))
                    {
                        await AtcState.FireAsync(Trigger.LineUpAndWait);
                    }
                    else
                    {
                        await AtcState.FireAsync(Trigger.ClearTakeoff);
                    }

                    return;
                }

                // This is a runway node we want to cross

                // Get the runways (each way) that contain the node we are at
                var crossedRunways = Airfield.RunwayNodes.Where(pair => pair.Value.Contains(CurrentNavigationPoint))
                    .Select(pair => pair.Key).ToList();
                // Check to see if we have any aircraft using the runway we want to cross
                if (Airfield.ControlledPlayers.Values.Any(x =>
                    StatesPreventingRunwayCrossing.Contains(x.CurrentState) && crossedRunways.Contains(x.Destination)))
                {
                    await AtcState.FireAsync(Trigger.HoldShort);
                }
                else
                {
                    await SendTransmission("cross runway at your discretion");
                }
            }
        }

        internal async Task CheckHoldingShort()
        {
            if (NavigationPoints.Count == 1)
            {
                if (Airfield.ControlledPlayers.Values.Any(otherAircraft =>
                        StatesRequiringHoldShort.Contains(otherAircraft.CurrentState) &&
                        otherAircraft.Destination == CurrentNavigationPoint))
                    // No-op, plane is already holding short so we don't need to tell them to hold short again.
                    return;
                if (Airfield.ControlledPlayers.Values.Any(otherAircraft =>
                    StatesRequiringLineUpAndWait.Contains(otherAircraft.CurrentState) &&
                    otherAircraft.Destination == CurrentNavigationPoint))
                {
                    await AtcState.FireAsync(Trigger.LineUpAndWait);
                }
                else
                {
                    await AtcState.FireAsync(Trigger.ClearTakeoff);
                }

                return;
            }

            var crossedRunways = Airfield.RunwayNodes.Where(pair => pair.Value.Contains(CurrentNavigationPoint))
                .Select(pair => pair.Key).ToList();
            if (!Airfield.ControlledPlayers.Values.Any(otherAircraft =>
                StatesPreventingRunwayCrossing.Contains(otherAircraft.CurrentState) &&
                crossedRunways.Contains(otherAircraft.Destination)))
            {
                await SendTransmission("cross runway at your discretion");
                await AtcState.FireAsync(Trigger.StartTaxi);
            }
        }

        private async Task CheckLinedUpAndWaiting()
        {
            if (!Airfield.ControlledPlayers.Values.Any(otherAircraft =>
                StatesRequiringLineUpAndWait.Contains(otherAircraft.CurrentState) &&
                otherAircraft.Destination == CurrentNavigationPoint))
            {
                await AtcState.FireAsync(Trigger.ClearTakeoff);
            }
        }

        private async Task CheckClearedTakeoff()
        {
            if (PlayerUnit.Speed > 30) // m/s
                await AtcState.FireAsync(Trigger.StartTakeoffRoll);
        }

        private async Task CheckTakeoffRoll()
        {
            if (PlayerUnit.Speed > 60 && PlayerUnit.Altitude > Airfield.Elevation + 33) // m/s and meters
                await AtcState.FireAsync(Trigger.Takeoff);
        }

        private async Task CheckInbound()
        {
            try {
                if (TimeToNextNavigationPoint().TotalSeconds < 10)
                {
                    if (NextNavigationPoint.Name.Contains("Entry"))
                    {
                        await AtcState.FireAsync(Trigger.TurnBase);
                        return;
                    }
                }
                if (_updateMessageSent == 0 && TimeToNextNavigationPoint().TotalSeconds is < 90 and >= 45 )
                {
                    if (NextNavigationPoint.Name.Contains("Entry"))
                    {
                        _updateMessageSent += 1;
                        await TransmitHeadingToNextWaypoint();
                    }
                }
                if (_updateMessageSent == 1 && TimeToNextNavigationPoint().TotalSeconds is < 45 and >= 20 )
                {
                    if (NextNavigationPoint.Name.Contains("Entry"))
                    {
                        _updateMessageSent += 1;
                        await TransmitHeadingToNextWaypoint(", descend and maintain 1000");
                    }
                }
            } catch(OverflowException)
            {
                // No-op
            }
        }

        private async Task CheckApproachingTurnFinal()
        {
            if (TimeToNextNavigationPoint().TotalSeconds < 25)
            {
                await AtcState.FireAsync(Trigger.TurnFinal);
            }
        }

        private async Task CheckApproachingShortFinal()
        {       
            if (TimeToNextNavigationPoint().TotalSeconds < 30)
            {
                await AtcState.FireAsync(Trigger.EnterShortFinal);
            }
        }

        private async Task CheckTouchdown()
        {
            if ((DateTime.Now - _lastLog).TotalSeconds >= 1)
            {
                _logger.Debug(
                    $"Altitude {PlayerUnit.Altitude}, Elevation {Airfield.Elevation}, Speed {PlayerUnit.Speed}");
                _lastLog = DateTime.Now;
            }

            if (PlayerUnit.Altitude < Airfield.Elevation + 10 && PlayerUnit.Speed < 40)
            {
                await AtcState.FireAsync(Trigger.Touchdown);
            }
        }

        private async Task CheckLeftRunway()
        {
            var closestPoint = Airfield.TaxiPoints
                .OrderBy(taxiPoint => taxiPoint.DistanceTo(PlayerUnit, DistanceUnit.M))
                .First();
            if ((DateTime.Now - _lastLog).TotalSeconds >= 1)
            {
                _logger.Debug(
                    $"Closest Point {closestPoint.Name}");
                _lastLog = DateTime.Now;
            }

            if (!closestPoint.Name.Contains("Runway"))
            {
                await AtcState.FireAsync(Trigger.LeaveRunway);
            }
        }

        #endregion

        #region Utility Methods

        /// <summary>
        ///     Returns the number of seconds until the player is expected to hit the next waypoint
        ///     based on current speed
        /// </summary>
        /// <returns>Timespan</returns>
        private TimeSpan TimeToNextNavigationPoint()
        {
            var closestPoint = NextNavigationPointLine.OrderBy(x => this.DistanceTo(x, DistanceUnit.Nm)).First();
            var distance = this.DistanceTo(closestPoint, DistanceUnit.M);

            var speed = PlayerUnit.Speed;
            var time = TimeSpan.FromSeconds(distance / speed);
            if (!((DateTime.Now - _lastLog).TotalSeconds >= 1)) return time;
            _logger.Debug($"Time to {NextNavigationPoint} is {time.TotalSeconds} seconds ({closestPoint.Name})");
            _lastLog = DateTime.Now;

            return time;
        }
        private DateTime _lastLog = DateTime.Now;

        #endregion
    }
}
