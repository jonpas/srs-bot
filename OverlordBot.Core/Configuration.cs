﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;

namespace RurouniJones.OverlordBot.Core
{
    public sealed class Configuration
    {

        public string Name { get; set; }
        public string TelemetryKey { get; set; }
        public AzureConfiguration Azure { get; set; }
        public DiscordConfiguration Discord { get; set; }
        public IEnumerable<GameServerConfiguration> GameServers { get; set; }

        public sealed class AzureConfiguration
        {
            public string Region { get; set; }
            public SpeechConfiguration Speech { get; set; }
            public LanguageUnderstandingConfiguration LanguageUnderstanding { get; set; }
        }

        public sealed class DiscordConfiguration
        {
            public string Token { get; set; }
            public ulong Server { get; set; }
            public bool CommandsEnabled { get; set; }
        }

        public sealed class SpeechConfiguration
        {
            public string SubscriptionKey { get; set; }
            public Guid EndpointId { get; set; }
        }

        public sealed class LanguageUnderstandingConfiguration
        {
            public Guid AppId { get; set; }
            public string Endpoint { get; set; }
            public string EndpointKey { get; set; }
        }

        public sealed class GameServerConfiguration
        {
            public enum DatastoreType
            {
                TacScribeDatabase,
                GrpcDatastore
            }

            public string Name { get; set; }
            public string ShortName { get; set; }
            public string Host { get; set; }
            public DatabaseConfiguration Database { get; set; }
            public SimpleRadioConfiguration SimpleRadio { get; set; }
            public RpcConfiguration Rpc { get; set; }
            public IEnumerable<ControllerConfiguration> Controllers { get; set; }
            public DatastoreType Datastore { get; set; }
        }

        public sealed class SimpleRadioConfiguration
        {
            public string Host { get; set; }
            public int Port { get; set; }
        }

        public sealed class RpcConfiguration
        {
            public int Port { get; set; }
        }

        public sealed class DatabaseConfiguration
        {
            public string Host { get; set; }
            public int Port { get; set; }
            public string Name { get; set; }
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public sealed class ControllerConfiguration
        {
            public enum ControllerType
            {
                Awacs,
                Atc,
                Mute
            }

            public string Name { get; set; }
            public double Frequency { get; set; }
            public string Modulation { get; set; }
            public ControllerType Type { get; set; }
            public string Callsign { get; set; }
            public string Password { get; set; }
            public string Voice { get; set; }
            public ulong DiscordLogChannel { get; set; }
            public bool RespondsToTransmissions { get; set; } = true;
            public string UnitPilot { get; set; } = string.Empty;
            public bool RestrictLineOfSight { get; set; } = false;
        }
    }
}
