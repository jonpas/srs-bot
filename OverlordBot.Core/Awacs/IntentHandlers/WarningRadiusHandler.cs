﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    internal class WarningRadiusHandler
    {
        public static Reply Process(ITransmission basicTransmission, Unit playerUnit, Reply reply)
        {
            var transmission = (SetWarningRadiusTransmission) basicTransmission;

            if (transmission.Distance >= 0)
            {
                if (transmission.Distance > 5) { 
                    var measurement = BogeyDopeHandler.MetricAirframes.Contains(playerUnit.Name) ? "kilometers" : "nautical miles";
                    reply.Message = $"copy, warning {transmission.Distance} {measurement}";
                } else
                {
                    reply.Message = "copy, merge calls only";
                }
                reply.Notes = "The bot will ignore any provided distance units (i.e. 'miles'/'kilometers') and instead use the same unit as the airframe of the transmitter. It might get smarter later.";
                reply.ContinueProcessing = true;
            }
            else
            {
                reply.Message = "i could not catch the distance";
                reply.ContinueProcessing = false;
            }

            return reply;
        }
    }
}