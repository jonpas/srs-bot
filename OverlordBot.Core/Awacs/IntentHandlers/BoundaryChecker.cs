﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using Geo.Measure;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    internal partial class WarningRadiusChecker
    {
        //TODO Dry this up with the BogeyDopeDetails. The only difference is "bra" vs "threat bra"
        internal record BoundaryDetails : Reply.IReplyDetails
        {
            private readonly int _bearing;
            private readonly int _range; // In kilometers
            private readonly int _altitude; // In meters
            private readonly int _speed;
            private readonly string _aspect;
            private readonly List<Unit> _group;
            private readonly Reply.IReplyDetails.MeasurementSystem _system;

            public BoundaryDetails(int bearing, int range, int altitude, string aspect, int speed, List<Unit> group, Reply.IReplyDetails.MeasurementSystem system)
            {
                _bearing = bearing;
                _range = range;
                _altitude = altitude;
                _aspect = aspect;
                _speed = speed;
                _group = group;
                _system = system;
            }

            public string ToSpeech()
            {
                var sections = new List<string>
                {
                    "threat",
                    "bra",
                    Reply.IReplyDetails.BearingToWords(_bearing),
                    Reply.IReplyDetails.RangeToWords(_range, _system),
                    Reply.IReplyDetails.AltitudeToWords(_altitude, _system),
                    _aspect
                };

                sections.AddRange(Reply.IReplyDetails.CommentsToWords(_altitude, _speed));
                sections.AddRange(Reply.IReplyDetails.UnitCountsToWords(_group));

                return string.Join(", ", sections);
            }
        }

        public static WarningCheckResult ProcessBoundaryContacts(Unit monitoredUnit, int distance,
            List<Unit> newContacts, List<Unit> reportedContacts)
        {
            var contacts = FilterReportedContacts(monitoredUnit, distance, 6, newContacts, reportedContacts);
            return contacts.Count == 0 ? new WarningCheckResult() : BuildBoundaryResponse(monitoredUnit, contacts, distance);
        }

        private static WarningCheckResult BuildBoundaryResponse(Unit playerUnit, List<Unit> contacts, int boundary)
        {
           var transmitterPoint = playerUnit.Location;

            contacts = contacts.ToList();

            var closestContact = contacts.First();

            var system = BogeyDopeHandler.MetricAirframes.Contains(playerUnit.Name)
                ? Reply.IReplyDetails.MeasurementSystem.Metric
                : Reply.IReplyDetails.MeasurementSystem.Imperial;

            var closestContactDistance = system == Reply.IReplyDetails.MeasurementSystem.Metric
                ? (int) playerUnit.DistanceTo(closestContact, DistanceUnit.Km)
                : (int)playerUnit.DistanceTo(closestContact, DistanceUnit.Nm);

            // Only report the closest contact & friends if the closest contact is within the warning distance
            if (closestContactDistance > boundary) return new WarningCheckResult();

            Logger.Debug($"Closest {closestContact}");

            var trueBearing = playerUnit.BearingTo(closestContact);

            var bearing = (int) Geospatial.TrueToMagnetic(transmitterPoint, trueBearing);
            var range = (int)playerUnit.DistanceTo(closestContact, DistanceUnit.Km);
            var altitude = (int) closestContact.Altitude;
            var aspect = GetAspect((int) trueBearing, closestContact.Heading);

            // ReSharper disable once PossibleUnintendedReferenceComparison
            var group = contacts.Where(contact => closestContact != contact &&
                                                  closestContact.DistanceTo(contact, DistanceUnit.Nm) < 5 &&
                                                  DiffHeading(closestContact.Heading, contact.Heading) < 90 &&
                                                  Math.Abs(playerUnit.DistanceTo(closestContact, DistanceUnit.Nm) - playerUnit.DistanceTo(contact,
                                                      DistanceUnit.Nm)) < 3
            ).ToList();
            group.Add(closestContact);

            Logger.Debug($"Newly reported contacts:\n{LogContacts(playerUnit, group)}");
            return new WarningCheckResult
            {
                Reply =  new Reply
                {
                    Details =  new BoundaryDetails(bearing, range, altitude, aspect, closestContact.Speed, group, system)
                },
                NewBoundaryContacts = group
            };
        }
    }
}