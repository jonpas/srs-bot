﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Geo.Measure;
using NLog;
using RurouniJones.OverlordBot.Core.Responders;
using RurouniJones.OverlordBot.Core.Util;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Awacs.IntentHandlers
{
    internal partial class WarningRadiusChecker
    {
        private readonly IPlayerRepository _playerRepository;
        private readonly IUnitRepository _unitRepository;

        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        private static readonly string[] CardinalPoints =
            {"NORTH", "NORTH EAST", "EAST", "SOUTH EAST", "SOUTH", "SOUTH WEST", "WEST", "NORTH WEST", "NORTH"};

        public object MergedChecker { get; private set; }

        public WarningRadiusChecker(IPlayerRepository playerRepository, IUnitRepository unitRepository)
        {
            _playerRepository = playerRepository;
            _unitRepository = unitRepository;
        }

        public async Task<WarningCheckResult> Process(Unit monitoredUnit, int distance,
            List<Unit> reportedMergedContacts, List<Unit> reportedBoundaryContacts, Unit awacsUnit, string serverShortName, bool losIsRestricted)
        {
            Logger.Trace($"Processing Warning Radius Check");
            var currentUnitData = await GetMonitoredPlayerUnitData(monitoredUnit, serverShortName);
            if (!Equals(monitoredUnit, currentUnitData)) {
                Logger.Trace($"Monitored unit and currentUnitData do not match, Skipping.\n{monitoredUnit}\n{currentUnitData}");
                return new WarningCheckResult
                {
                    Deleted = true
                };
            }

            Logger.Trace($"Checking Warning Radius within {distance} miles of {currentUnitData.Pilot}");

            // Assume player is on ground and do not warn because many players might be at the same airbase.
            if (currentUnitData.Speed < 10)
            {
                Logger.Trace($"{currentUnitData.Pilot} is stationary. Skipping check");
                return new WarningCheckResult();
            }

            var allReportedContacts = reportedBoundaryContacts;
            allReportedContacts.AddRange(reportedMergedContacts);

            var allContacts = await _unitRepository.FindHostileAircraft(currentUnitData.Location, currentUnitData.Coalition,
                null, serverShortName);

            // Remove all merged contacts that are no longer in-game
            for (var index = reportedMergedContacts.Count - 1; index >= 0; index--)
            {
                var mergedContact = reportedMergedContacts[index];
                if (allContacts.Select(x => x.Id).Contains(mergedContact.Id)) continue;
                reportedMergedContacts.RemoveAt(index);
                Logger.Trace($"Removed contact {mergedContact}");
            }

            // Remove all boundary contacts that are no longer in-game
            for (var index = reportedBoundaryContacts.Count - 1; index >= 0; index--)
            {
                var boundaryContact = reportedBoundaryContacts[index];
                if (allContacts.Select(x => x.Id).Contains(boundaryContact.Id)) continue;
                reportedBoundaryContacts.RemoveAt(index);
                Logger.Trace($"Removed contact {boundaryContact}");
            }

            Logger.Trace($"Merged Contacts Report:\n{LogContacts(currentUnitData, allContacts, reportedMergedContacts)}");
            Logger.Trace($"Boundary Contacts Report:\n{LogContacts(currentUnitData, allContacts, reportedBoundaryContacts)}");

            if (awacsUnit != null && losIsRestricted)
            {
                allContacts.RemoveAll(contact => !LineOfSightChecker.HasLineOfSight(awacsUnit, contact));
                Logger.Trace($"Visible Contacts:\n{LogContacts(currentUnitData, allContacts, allReportedContacts)}");
            }

            var result = ProcessMergedContacts(currentUnitData, allContacts, reportedMergedContacts);

            Logger.Trace($"New merged contacts reported this check: {result.NewMergedContacts.Count}");
            Logger.Trace($"New boundary contacts reported this check: {result.NewBoundaryContacts.Count}");

            if (result.NewMergedContacts.Count > 0) {
                Logger.Trace($"New merged contact reported. Skipping boundary reports");
                return result;
            } else if(distance >= MergedBoundary)
            {
                Logger.Trace($"Tripwire distance {distance} is above merged distance {MergedBoundary}. Reporting boundary reports");
                return ProcessBoundaryContacts(currentUnitData, distance, allContacts, allReportedContacts);
            } else
            {
                Logger.Trace($"Tripwire distance {distance} is below merged distance {MergedBoundary}. Skipping boundary reports");
                return new WarningCheckResult();
            }
        }

        private static string LogContacts(Unit player, IEnumerable<Unit> contacts, ICollection<Unit> reportedContacts = null)
        {
            var location = player.Location;
            var sb = new StringBuilder();
            foreach (var contact in contacts)
            {
                var bearing = Geospatial.TrueToMagnetic(location, player.BearingTo(contact)).ToString("000");
                var range = (int) player.DistanceTo(contact, DistanceUnit.Nm);

                string reported = null;
                if (reportedContacts != null && reportedContacts.Contains(contact)) reported = "(Reported)";
                sb.AppendLine($"{bearing}/{range}: {contact.Id}, {contact.Name}, {contact.Group} {reported}");
            }
            return sb.ToString();
        }

        private static List<Unit> FilterReportedContacts(Unit player, int distance, int buffer, IEnumerable<Unit> contacts, IEnumerable<Unit> reportedContacts)
        {
            var contactsInRange = contacts
                .Where(contact => !reportedContacts.Contains(contact))
                // Include an extra miles to cover aircraft that might be in the group of the closest target but not yet within the maximum.
                .Where(contact => player.DistanceTo(contact, DistanceUnit.Nm) < distance + buffer)
                .ToList();
            Logger.Trace($"Unreported Contacts in range:\n{LogContacts(player, contactsInRange)}");
            return contactsInRange;
        }

        /// <summary>
        ///     Return aspect information for a bogey dope according to ATP 3-52.4, Page 25
        /// </summary>
        /// <param name="bearing">Bearing from the friendly to the contact</param>
        /// <param name="heading">Heading of the contact</param>
        /// <returns></returns>
        private static string GetAspect(int bearing, int heading)
        {
            var angleAspect = DiffHeading(bearing, heading);

            if (heading < 0) heading += 360;

            var sb = new StringBuilder();

            switch (angleAspect)
            {
                case <= 65:
                    sb.Append("DRAG ");
                    sb.Append(CardinalPoints[(int) Math.Round((double) heading % 360 / 45)]);
                    break;
                case < 115:
                    sb.Append("BEAM ");
                    sb.Append(CardinalPoints[(int) Math.Round((double) heading % 360 / 45)]);
                    break;
                case < 155:
                    sb.Append("FLANK ");
                    sb.Append(CardinalPoints[(int) Math.Round((double) heading % 360 / 45)]);
                    break;
                default:
                    sb.Append("HOT ");
                    break;
            }

            return sb.ToString();
        }

        private static int DiffHeading(int a, int b)
        {
            return Math.Min(a - b < 0 ? a - b + 360 : a - b, b - a < 0 ? b - a + 360 : b - a);
        }

        private async Task<Unit> GetMonitoredPlayerUnitData(Unit playerUnit, string serverShortName)
        {
            return await _playerRepository.FindById(playerUnit.Id);
        }

        public class WarningCheckResult
        {
            public bool Deleted { get; set; } = false;
            public Reply Reply { get; set; } = null;
            public List<Unit> NewMergedContacts { get; set; } = new();
            public List<Unit> NewBoundaryContacts { get; set; } = new();
        }
    }
}