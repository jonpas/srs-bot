﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.Extensions.DependencyInjection;
using RurouniJones.Dcs.Grpc.V0.Mission;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace RurouniJones.OverlordBot.Core
{
    public  class GameServerFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public GameServerFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        // https://stackoverflow.com/questions/53884417/net-core-di-ways-of-passing-parameters-to-constructor
        public GameServer CreateGameServer(Configuration.GameServerConfiguration gameServerConfig, ulong discordLogServerId, CancellationToken stoppingToken)
        {
            var datastoreType = gameServerConfig.Datastore;

            if (datastoreType == Configuration.GameServerConfiguration.DatastoreType.GrpcDatastore)
            {
                var host = gameServerConfig.Host;
                var port = gameServerConfig.Rpc.Port;

                ConcurrentQueue<StreamUnitsResponse> unitStream = new();

                var dataSource = ActivatorUtilities.CreateInstance<GrpcDatastore.DataSource>(_serviceProvider, host, port, unitStream);

                return ActivatorUtilities.CreateInstance<GameServer>(_serviceProvider,
                    gameServerConfig,
                    dataSource,
                    discordLogServerId,
                    new GrpcDatastore.AirfieldRepository(dataSource),
                    new GrpcDatastore.PlayerRepository(dataSource),
                    new GrpcDatastore.UnitRepository(dataSource),
                    stoppingToken);
            } else
            {
                var host = gameServerConfig.Database.Host;
                var port = gameServerConfig.Database.Port;
                var name = gameServerConfig.Database.Name;
                var username = gameServerConfig.Database.Username;
                var password = gameServerConfig.Database.Password;

                var dataSource = ActivatorUtilities.CreateInstance<TacScribeDatabase.DataSource>(_serviceProvider, host, port, name, username, password);

                return ActivatorUtilities.CreateInstance<GameServer>(_serviceProvider,
                    gameServerConfig,
                    dataSource,
                    discordLogServerId,
                    new TacScribeDatabase.AirfieldRepository(dataSource),
                    new TacScribeDatabase.PlayerRepository(dataSource),
                    new TacScribeDatabase.UnitRepository(dataSource),
                    stoppingToken);
            }
        }
    }
}
