﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Threading.Tasks;
using Geo.Geometries;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using RurouniJones.OverlordBot.Cognitive.LanguageUnderstanding.Transmissions;
using RurouniJones.OverlordBot.Core.Tests.Mocks;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.Core.Tests.AwacsResponder.Intents
{
    /// <summary>
    /// Verify the bot responds correctly to location of entity requests for units.
    /// </summary>
    [TestClass]
    public class LocationOfUnitTests
    {
        [TestMethod]
        public async Task WhenTransmitterIsImperial_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(0, 0),
                    Coalition = 1,
                    Name = "F-15C"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindByCallsignAndCoalitionResult = new Unit
                {
                    Location = new Point(0.2, 0),
                    Altitude = 1000,
                    Coalition = 1,
                    Pilot = "DOLT 1-3"
                },
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var subject = new ITransmission.Player("dolt", 1, 2);
            var transmission = new LocationOfEntityTransmission(null, ITransmission.Intents.LocationOfEntity, player, awacs, subject);

            const string expectedResponse = "dolt 1 2, overlord, Bra, 0 0 4, 11, Angels 3";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }

        [TestMethod]
        public async Task WhenTransmitterIsMetric_ThenReturnsCorrectResponse()
        {
            var playerRepository = new MockPlayerRepository
            {
                FindByCallSignResult = new Unit
                {
                    Location = new Point(0, 0),
                    Coalition = 1,
                    Name = "MiG-29A"
                }
            };

            var unitRepository = new MockUnitRepository
            {
                FindByCallsignAndCoalitionResult = new Unit
                {
                    Location = new Point(0.2, 0),
                    Altitude = 1000,
                    Coalition = 1,
                    Pilot = "DOLT 1-3"
                },
            };

            var controller = new Awacs.AwacsResponder(playerRepository, unitRepository, new MockAirfieldRepository(), false, null, "TEST");

            var player = new ITransmission.Player("dolt", 1, 2);
            var awacs = new ITransmission.Awacs("Overlord");
            var subject = new ITransmission.Player("dolt", 1, 2);
            var transmission = new LocationOfEntityTransmission(null, ITransmission.Intents.LocationOfEntity, player, awacs, subject);

            const string expectedResponse = "dolt 1 2, overlord, Bra, 0 0 4, 22, Angels 1";
            var actualResponse = await controller.ProcessTransmission(transmission);

            Assert.AreEqual(expectedResponse, actualResponse.ToSpeech());
        }
    }
}
