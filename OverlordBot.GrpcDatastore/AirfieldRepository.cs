﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geo.Geometries;
using Grpc.Net.Client;
using NLog;
using RurouniJones.OverlordBot.Datastore;
using Unit = RurouniJones.OverlordBot.Datastore.Models.Unit;
using Geo.Geodesy;
using RurouniJones.Dcs.Grpc.V0.Atmosphere;
using RurouniJones.Dcs.Grpc.V0.World;
using RurouniJones.Dcs.Grpc.V0.Common;

namespace RurouniJones.OverlordBot.GrpcDatastore
{
    public class AirfieldRepository : IAirfieldRepository
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();

        private readonly DataSource _dataSource;
        public AirfieldRepository(DataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public ConcurrentDictionary<string, Unit> Airfields { get; set; } = new();

        public async Task<IEnumerable<Unit>> GetAllLandAirfields(string serverShortName)
        {
            using var channel = GrpcChannel.ForAddress($"http://{_dataSource.Host}:{_dataSource.Port}");
            var worldClient = new WorldService.WorldServiceClient(channel);
            var airbases = await worldClient.GetAirbasesAsync(new GetAirbasesRequest {});

            var atmoClient = new AtmosphereService.AtmosphereServiceClient(channel);

            List<Unit> convertedAirbases = new();
            foreach(var airbase in airbases.Airbases)
            {
                if(airbase.Category != AirbaseCategory.Airdrome) continue;

                var windMeasurePoint = airbase.Position;
                windMeasurePoint.Alt = windMeasurePoint.Alt + 10;
                var wind = await atmoClient.GetWindAsync(new GetWindRequest
                {
                    Position = airbase.Position,
                });

                var unit = new Unit
                {
                    Id = airbase.Name.GetHashCode().ToString(),
                    Altitude = airbase.Position.Alt,
                    Coalition = (int) airbase.Coalition - 1,// Minus one to deal with gRPC enum offset.
                    Location = new Point(airbase.Position.Lat, airbase.Position.Lon),
                    Name = airbase.Name,
                    Callsign = airbase.DisplayName,
                    Heading = (int) wind.Heading,
                    Speed = (int) (wind.Strength * 1.943844) // m/s to knots.
                };
                Airfields[unit.Id] = unit;

                Logger.Trace($"Processed airfield update: {unit}");
            }
            return Airfields.Values;
        }

        public async Task<Unit> FindByName(string name, string serverShortName)
        {
            return Airfields.Values.First(a => a.Name == name);
        }

        public async Task<Unit> FindByClosest(Point sourcePosition, int coalition, string serverShortName)
        {
            return Airfields.Values.OrderBy(a => a.Location.CalculateGreatCircleLine(sourcePosition).Distance).First();
        }
    }
}
