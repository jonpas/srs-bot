﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.Collections.Concurrent;

namespace RurouniJones.OverlordBot.Administration
{
    public static class CommandQueue
    {
        public static readonly ConcurrentQueue<IAdminCommand> Commands = new();

        public interface IAdminCommand
        {
        }

        public class EnableControllerCommand : IAdminCommand 
        {
            public Guid Id;

            public EnableControllerCommand(Guid id)
            {
                Id = id;
            }
        }

        public class DisableControllerCommand : IAdminCommand
        {
            public Guid Id;

            public DisableControllerCommand(Guid id)
            {
                Id = id;
            }
        }
    }
}
