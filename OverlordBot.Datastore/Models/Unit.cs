﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Geo.Geometries;
using System;
using System.Text;

namespace RurouniJones.OverlordBot.Datastore.Models
{
    public record Unit
    {
        /// <summary>
        /// DCS unit ID. can be a hex string if the source is Tacview or an
        /// integer string if from DCS-gRPC for example.
        /// </summary>
        public string Id { get; init; }

        /// <summary>
        /// Altitude above sea level in meters
        /// </summary>
        public double Altitude { get; set; }

        /// <summary>
        /// 0 = neutral, 1 = redfor, 2 = bluefor
        /// </summary>
        public int Coalition { get; init; }

        /// <summary>
        /// Name of the DCS group the unit belongs to
        /// </summary>
        public string Group { get; init; }

        /// <summary>
        /// True heading of the unit as far as DCS is concerned.
        /// </summary>
        public int Heading { get; set; }

        /// <summary>
        /// The 2D location of the unit. We store altitude separately.
        /// </summary>
        public Point Location { get; set; }

        /// <summary>
        ///  name of the Unit. E.g. "MiG-29S"
        /// </summary>
        public string Name { get; init; }

        /// <summary>
        /// Name of the pilot
        /// </summary>
        public string Pilot { get; init; }


        /// <summary>
        ///  DCS assigned callsign of the unit
        /// </summary>
        public string Callsign { get; init; }

        /// <summary>
        /// Speed in meters per second
        /// </summary>
        public int Speed { get; set; }

        /// <summary>
        /// The Mil-STD-2525-D code for the unit
        /// </summary>
        public MilStd2525d MilStd2525d { get; init; }
        
        public virtual bool Equals(Unit unit2)
        {
            return unit2 != null &&
                Id == unit2.Id
                && Coalition == unit2.Coalition
                && Group == unit2.Group
                && Name == unit2.Name
                && Pilot == unit2.Pilot;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Id, Coalition, Group, Name, Pilot);
        }

        public override string ToString()
        {
            return string.Join(Id, Coalition, Group, Name, Pilot);
        }

    }
}