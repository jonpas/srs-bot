﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NLog;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.TacScribeDatabase
{
    public class UnitRepository : IUnitRepository
    {
        private readonly Logger _logger = LogManager.GetCurrentClassLogger();
        private static readonly List<string> AwacsCodes = new() {"A-50","E-2D","E-3A","KJ-2000"};
        private readonly DataSource _dataSource;

        public UnitRepository(DataSource dataSource)
        {
            _dataSource = dataSource;
        }
        public async Task<List<Unit>> FindHostileAircraft(Geo.Geometries.Point sourcePosition, int coalition, List<string> aircraftCodes, string serverShortName)
        {
            var opposingCoalition = coalition == 1 ? 2 : 1;

            await using var db = _dataSource.Context;
            var query =
                from bogey in db.Units
                where bogey.Coalition == opposingCoalition
                where bogey.Speed > 25
                where bogey.Type.StartsWith("Air")
                where bogey.Deleted == false
                select bogey;

                if (aircraftCodes is { Count: > 0 })
                {
                    query = query.Where(bogey => aircraftCodes.Contains(bogey.Name));
                }

                var point = new NetTopologySuite.Geometries.Point(sourcePosition.Coordinate.Longitude, sourcePosition.Coordinate.Latitude);
                query = query.OrderBy(bogey => bogey.Position.Distance(point));
            return new List<Unit>(query.ToList().Select(u => u.ToGenericUnit()).ToList());
        }

        public async Task<Unit> FindAwacs(string pilot, string serverShortName)
        {
            await using var db = _dataSource.Context;
            var query =
                from awacs in db.Units
                where awacs.Pilot.StartsWith(pilot)
                where AwacsCodes.Contains(awacs.Name)
                where awacs.Deleted == false
                select awacs;
            return query.FirstOrDefault()?.ToGenericUnit();
        }

        public async Task<Unit> FindByCallsignAndCoalition(string groupName, int flight, int element, int coalition)
        {
            await using var db = _dataSource.Context;
            var query =
                from bogey in db.Units
                where PilotOrCallsignMatches(bogey, groupName, flight, element)
                where bogey.Coalition == coalition
                where bogey.Speed > 25
                where bogey.Type.StartsWith("Air")
                where bogey.Deleted == false
                select bogey;

            var matches = query.ToList();

            if (matches.Count() > 1)
            {
                foreach (Models.Unit unit in matches)
                    _logger.Warn(unit);
                throw new DuplicateCallsignException(groupName, flight, element);
            }

            return matches.Select(u => u.ToGenericUnit()).FirstOrDefault();
        }

        private static bool PilotOrCallsignMatches(Models.Unit u, string groupName, int flight, int element)
        {
            return u.Pilot.ToLower().Contains($"{groupName.ToLower()} {flight}-{element}") ||
                   u.Pilot.ToLower().Contains($"{groupName.ToLower()} {flight}{element}");
        }
    }
}