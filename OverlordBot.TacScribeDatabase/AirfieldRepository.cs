﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Geo.Geometries;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.TacScribeDatabase
{
    public class AirfieldRepository : IAirfieldRepository
    {
        private static readonly List<string> AirfieldTypes = new()
        {
            "Ground+Static+Aerodrome",
            "Sea+Watercraft+AircraftCarrier"
        };

        private readonly DataSource _dataSource;

        public AirfieldRepository(DataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public async Task<IEnumerable<Unit>> GetAllLandAirfields(string serverShortName)
        {
            await using var db = _dataSource.Context;
            var query =
                from airfield in db.Units
                where airfield.Type.Equals("Ground+Static+Aerodrome")
                select airfield;
            return query.ToList().Select(u => u.ToGenericUnit()).ToList();
        }

        public async Task<Unit> FindByName(string name, string serverShortName)
        {
            await using var db = _dataSource.Context;
            var query =
                from airfield in db.Units
                where AirfieldTypes.Contains(airfield.Type)
                where airfield.Name.ToLower().Equals(name.ToLower())
                select airfield;
            return query.FirstOrDefault()?.ToGenericUnit();
        }

        public async Task<Unit> FindByClosest(Point sourcePosition, int coalition, string serverShortName)
        {
            await using var db = _dataSource.Context;
            var query =
                from airfield in db.Units
                where AirfieldTypes.Contains(airfield.Type)
                where airfield.Coalition == coalition
                orderby airfield.Position.Distance(new NetTopologySuite.Geometries.Point(sourcePosition.Coordinate.Longitude, sourcePosition.Coordinate.Latitude))
                select airfield;
            return query.FirstOrDefault()?.ToGenericUnit();
        }
    }
}