﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using NLog;
using Npgsql.Logging;

namespace RurouniJones.OverlordBot.TacScribeDatabase.Util
{
    //NLog logging provider for Npgsql
    public class NLogLoggingProvider : INpgsqlLoggingProvider
    {
        public NpgsqlLogger CreateLogger(string name)
        {
            return new NLogLogger();
        }
    }

    internal class NLogLogger : NpgsqlLogger
    {
        private readonly Logger _logger = LogManager.GetLogger("RurouniJones.OverlordBot.DataStore.Npgsql");

        public override bool IsEnabled(NpgsqlLogLevel level)
        {
            return _logger.IsEnabled(ToNLogLogLevel(level));
        }

        public override void Log(NpgsqlLogLevel level, int connectorId, string msg, Exception exception = null)
        {
            var ev = new LogEventInfo(ToNLogLogLevel(level), "RurouniJones.OverlordBot.DataStore.Npgsql", msg);
            if (exception != null)
                ev.Exception = exception;
            if (connectorId != 0)
                ev.Properties["ConnectorId"] = connectorId;
            _logger.Log(ev);
        }

        private static LogLevel ToNLogLogLevel(NpgsqlLogLevel level)
        {
            return level switch
            {
                NpgsqlLogLevel.Trace => LogLevel.Trace,
                NpgsqlLogLevel.Debug => LogLevel.Debug,
                NpgsqlLogLevel.Info => LogLevel.Info,
                NpgsqlLogLevel.Warn => LogLevel.Warn,
                NpgsqlLogLevel.Error => LogLevel.Error,
                NpgsqlLogLevel.Fatal => LogLevel.Fatal,
                _ => throw new ArgumentOutOfRangeException(nameof(level))
            };
        }
    }
}
