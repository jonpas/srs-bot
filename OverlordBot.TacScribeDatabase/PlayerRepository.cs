﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.Datastore.Models;

namespace RurouniJones.OverlordBot.TacScribeDatabase
{
    public class PlayerRepository : IPlayerRepository
    {
        private readonly DataSource _dataSource;

        public PlayerRepository(DataSource dataSource)
        {
            _dataSource = dataSource;
        }

        public async Task<Unit> FindByCallsign(string groupName, int flight, int element)
        {
            await using var db = _dataSource.Context;
            var matches = db.Units.Where(unit => unit.Pilot.ToLower().Contains($"{groupName} {flight}-{element}") || unit.Pilot.ToLower().Contains($"{groupName} {flight}{element}")).ToList();
            if (matches.Count > 1)
            {
                throw new DuplicateCallsignException(groupName, flight, element);
            }
            else
            {
                var unit = matches.FirstOrDefault();
                return unit?.ToGenericUnit();
            }           
        }

        public async Task<Unit> FindByCallsignAndCoalition(string groupName, int flight, int element, int coalition)
        {
            await using var db = _dataSource.Context;
            var u = await db.Units.FirstOrDefaultAsync(
                unit => (unit.Pilot.ToLower().Contains($"{groupName} {flight}-{element}") 
                         || unit.Pilot.ToLower().Contains($"{groupName} {flight}{element}")) 
                        && unit.Coalition == coalition);
            return u?.ToGenericUnit();
        }

        public async Task<Unit> FindById(string id)
        {
            await using var db = _dataSource.Context;
            var u = await db.Units.FirstOrDefaultAsync(unit => unit.Id == id);
            return u?.ToGenericUnit();
        }
    }
}
