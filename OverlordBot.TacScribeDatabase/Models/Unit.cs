﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using NetTopologySuite.Geometries;

namespace RurouniJones.OverlordBot.TacScribeDatabase.Models
{

    public sealed record Unit
    {
        #region Used outside of DataStore

        public string Id { get; init; }

        public double Altitude { get; set; }

        public int Coalition { get; init; }

        public string Group { get; init; }

        public int Heading { get; set; }

        /// <summary>
        /// It is not ideal having Location AND Position but we need to keep position around
        /// for Entity Framework purposes. Having location as part of the model saves us
        /// a lot of low-value code elsewhere since the Core uses Geo for calculations.
        /// </summary>
        public  Geo.Geometries.Point Location => new(Position.Y, Position.X);
        
        public string Name { get; init; }
        
        public string Pilot { get; init; }
        
        public int Speed { get; set; }
        
        #endregion

        #region Used only in Datastore package

        [Column(TypeName="geography (point)")]
        public Point Position { get; set; }

        /// <summary>
        /// DCS unit type. e.g "Air+FixedWing+"
        /// </summary>
        public string Type { get; init; }
        
        /// <summary>
        /// TacScribe sets this field before it is finally cleaned up async
        /// </summary>
        public bool Deleted { get; set; }

        /// <summary>
        ///  Time this record was last updated
        /// </summary>
        public DateTime UpdatedAt { get; set; }

        #endregion
        
        // God I wish C# had a way of doing this that was easy *and* not computationally expensive
        public override string ToString()
        {
            var sb = new StringBuilder("Unit, ");
            sb.Append($"Altitude: {Altitude}, ");
            sb.Append($"Coalition: {Coalition}, ");
            sb.Append($"Deleted: {Deleted}, ");
            sb.Append($"Group: {Group}, ");
            sb.Append($"Heading: {Heading}, ");
            sb.Append($"Id: {Id}, ");
            sb.Append($"Name: {Name}, ");
            sb.Append($"Pilot: {Pilot}, ");
            sb.Append($"Position: {Position}, ");
            sb.Append($"Speed: {Speed}, ");
            sb.Append($"Type: {Type}, ");
            sb.Append($"Updated At: {UpdatedAt}");

            return sb.ToString();
        }

        public bool Equals(Unit other)
        {
            if (other == null) return false;
            return Coalition == other.Coalition && Group == other.Group && Id == other.Id && Name == other.Name && Pilot == other.Pilot && Type == other.Type;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Coalition, Group, Id, Name, Pilot, Type);
        }

        public Datastore.Models.Unit ToGenericUnit()
        {
            return new Datastore.Models.Unit
            {
                Id = Id,
                Coalition = Coalition,
                Group = Group,
                Name = Name,
                Pilot = Pilot,
                Location = Location,
                Altitude = Altitude,
                Speed = Speed,
                Heading = Heading,
                MilStd2525d = new Datastore.Models.MilStd2525d(Coalition, Encyclopedia.MilStd2525d.GetUnitByDcsCode(Name).MilStdCode)
            };
        }
    }
}