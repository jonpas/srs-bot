﻿// OverlordBot is an AWACS/ATC bot for DCS World
// Copyright (C) 2022 Jeffrey Jones
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY

using Microsoft.EntityFrameworkCore;
using Npgsql;
using Npgsql.Logging;
using RurouniJones.OverlordBot.Datastore;
using RurouniJones.OverlordBot.TacScribeDatabase.Models;
using RurouniJones.OverlordBot.TacScribeDatabase.Util;

namespace RurouniJones.OverlordBot.TacScribeDatabase
{
    public class DataSource : IDataSource
    {
        public DbSet<Unit> Units { get; set; }

        public readonly DbContextOptionsBuilder _optionsBuilder;

        public GameContext Context => new GameContext(_optionsBuilder.Options);

        private readonly string _connectionString;

        public DataSource(string host, int port, string name, string username, string password)
        {
            NpgsqlConnection.GlobalTypeMapper.UseNetTopologySuite(geographyAsDefault: true);
            NpgsqlLogManager.Provider = new NLogLoggingProvider();
            NpgsqlLogManager.IsParameterLoggingEnabled = true;

            _connectionString = new NpgsqlConnectionStringBuilder
            {
                Host = host,
                Port = port,
                Database = name,
                Username = username,
                Password = password
            }.ConnectionString;

            _optionsBuilder = new DbContextOptionsBuilder()
                .UseNpgsql(_connectionString, o => o.UseNetTopologySuite())
                .UseSnakeCaseNamingConvention();
        }

        public class GameContext : DbContext
        {
            public DbSet<Unit> Units { get; set; }

            public GameContext(DbContextOptions options) : base(options) { }
        }
    }
}
